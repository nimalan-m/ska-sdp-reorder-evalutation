# DP3 + WSClean MS Reorder Evaluation

1. Baseline: DP3 run with averager, outputs averaged MS. WSClean is run with reorder and made to image the measurement set.
2. Reorder: DP3 run with averager and reorder, outputs reordered files. WSClean is run to image with the reordered files.

# Workflow and Setup

## Data

1. ASKAP: EMU Pilot Survey, Observation ID: SB40625, beam09, http://hdl.handle.net/102.100.100/164553?index=1
2. MWA: https://support.astron.nl/software/ci_data/IDG/MWA-1052736496-averaged.ms.tgz

## Nextflow

`baseline.nf` is a workflow for the baseline DP3 + WSClean. `reorder_flow.nf` is a workflow with the reorder changes. The definition of each process is defined in `steps/main.nf`

### To run for a new dataset

Add a parset for DP3 in the `config` folder and add the data in the `data` folder. From the nextflow directory run

```sh
# Baseline
./profile report_baseline_mwa.csv nextflow run baseline.nf --msname=MWA-1052736496-averaged.ms --parset_in=MWA_DP3.in

# Reorder
nextflow run reorder_flow.nf --msname=MWA-1052736496-averaged.ms --parset_in=MWA_DP3_reorder.in
```

## Profiling

https://github.com/scottchiefbaker/dool

### Profiling a single run

To run profiling for a single execution run

```sh
cd nextflow

# Profiling
# ./profile <name-of-dool-report> <command>

# Baseline
./profile report_baseline_mwa.csv nextflow run baseline.nf --msname=MWA-1052736496-averaged.ms --parset_in=MWA_DP3.in

# Reorder
./profile report_reorder_mwa.csv nextflow run reorder_flow.nf --msname=MWA-1052736496-averaged.ms --parset_in=MWA_DP3_reorder.in

# To create a visualization
# python dool-visu_scoop.py --report <name-of-dool-report>
python dool-visu_scoop.py --report report_baseline_mwa.csv
```

### Profiling for all the datasets

```sh
cd nextflow
sh run-all.sh
```
