#!/usr/bin/env nextflow

// Reference workflow
// 1. use wsclean to save reorder files
// 2. image using the reorder files wsclean

include { ws_clean_save_reorder; ws_clean_imaging_with_prereorder } from "./steps"

msname     = 'L628614_SAP004_SB340_uv_001.MS'
parset_in   = 'SB340_DP3.in'
resolution = '1.5arcsec'

// msname     = 'MWA-1052736496-averaged.ms'
// parset_in   = 'dp3.in'
// resolution = '0.05arcmin'

workflow {
    datafile   = Channel.fromPath('../data/' + msname)
    dp3_parset = Channel.fromPath('../config/' + parset_in)

    ws_clean_save_reorder(datafile, resolution)
    ws_clean_imaging_with_prereorder(datafile, resolution, ws_clean_save_reorder.out)
}
