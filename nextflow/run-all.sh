#!/usr/bin/sh

set -ex

./profile report_baseline_mwa.csv nextflow run baseline.nf --msname=MWA-1052736496-averaged.ms --parset_in=MWA_DP3.in
./profile report_baseline_lofar.csv nextflow run baseline.nf --msname=L628614_SAP004_SB340_uv_001.MS --parset_in=SB340_DP3.in
./profile report_baseline_askap.csv nextflow run baseline.nf --msname=scienceData.EMU_1554-55_band2.SB40625.EMU_1554-55_band2.beam09_averaged_cal.leakage.ms --parset_in=SB40625_DP3.in
./profile report_baseline_midband.csv nextflow run baseline.nf --msname=midbands.ms --parset_in=midband_DP3.in

./profile report_reorder_mwa.csv nextflow run reorder_flow.nf --msname=MWA-1052736496-averaged.ms --parset_in=MWA_DP3_reorder.in
./profile report_reorder_lofar.csv nextflow run reorder_flow.nf --msname=L628614_SAP004_SB340_uv_001.MS --parset_in=SB340_DP3_reorder.in
./profile report_reorder_askap.csv nextflow run reorder_flow.nf --msname=scienceData.EMU_1554-55_band2.SB40625.EMU_1554-55_band2.beam09_averaged_cal.leakage.ms --parset_in=SB40625_DP3_reorder.in
./profile report_reorder_midband.csv nextflow run reorder_flow.nf --msname=midbands.ms --parset_in=midband_DP3_reorder.in
