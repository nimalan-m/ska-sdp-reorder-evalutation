#!/usr/bin/env nextflow

include { dp3; ws_clean_imaging } from './steps'

params.msname     = 'scienceData.EMU_1554-55_band2.SB40625.EMU_1554-55_band2.beam09_averaged_cal.leakage.ms'
params.parset_in   = 'SB40625_DP3.in'
params.resolution = '1.5arcsec'

// msname     = 'L628614_SAP004_SB340_uv_001.MS'
// parset_in   = 'SB340_DP3.in'
// resolution = '1.5arcsec'

// msname     = 'MWA-1052736496-averaged.ms'
// parset_in   = 'dp3.in'
// resolution = '0.05arcmin'

workflow {
    datafile   = Channel.fromPath('../data/' + params.msname)
    dp3_parset = Channel.fromPath('../config/' + params.parset_in)

    dp3(datafile, dp3_parset)
    ws_clean_imaging(dp3.out[0], params.resolution)
}
