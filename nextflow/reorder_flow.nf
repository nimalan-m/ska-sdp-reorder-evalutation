#!/usr/bin/env nextflow

include { dp3_with_reorder; ws_clean_imaging_with_prereorder } from './steps'

params.msname     = 'scienceData.EMU_1554-55_band2.SB40625.EMU_1554-55_band2.beam09_averaged_cal.leakage.ms'
params.parset_in   = 'SB40625_DP3_reorder.in'
params.resolution = '1.5arcsec'

// msname     = 'L628614_SAP004_SB340_uv_001.MS'
// parset_in   = 'SB340_DP3_reorder.in'
// resolution = '1.5arcsec'

// msname     = 'MWA-1052736496-averaged.ms'
// parset_in   = 'dp3_reorder.in'
// resolution = '0.05arcmin'

workflow {
    datafile   = Channel.fromPath('../data/' + params.msname)
    dp3_parset = Channel.fromPath('../config/' + params.parset_in)

    dp3_with_reorder(datafile, dp3_parset)
    ws_clean_imaging_with_prereorder(dp3_with_reorder.out[0], params.resolution, dp3_with_reorder.out[1])
}
