#!/usr/bin/env python3

# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 16:25:46

@author: SCOOP team
"""

import argparse
import csv
import matplotlib.pyplot as plt
import numpy as np
from os.path import abspath

parser = argparse.ArgumentParser()
parser.add_argument("--report", "-r", type=str, help="Report path")
parser.add_argument("--cpu", action="store_true", help="Visualize CPU")
parser.add_argument("--cpu-all", action="store_true", help="Visualize all CPU cores")
parser.add_argument("--mem", action="store_true", help="Visualize memory")
parser.add_argument("--net", action="store_true", help="Visualize network")
parser.add_argument("--io", action="store_true", help="Visualize io")
parser.add_argument("--dpi", type=str, default="medium", help="Quality of figure")
args = parser.parse_args()
filename = args.report

class dool_visualizer():
    """
    Dool visualizer
    """
    def __init__(self):
        """
        Init
        """
        self.csv_report = []
        self.with_io = True
        self.prof = {}
        self.prof_keys = []
        self.ncpu = 0

        self._stamps = np.array([])
        self._xticks = ()
        self._yrange = 0
        self._zeros = np.array([])

        self.parse_csv_report()
        self.create_profile()
        self.create_plt_params()


    def parse_csv_report(self):
        """
        Parse the csv report
        """
        self.csv_report = []
        with open(filename, newline="") as csvfile:
            csvreader = csv.reader(csvfile)
            for row in csvreader:
                self.csv_report.append(row)

        version = self.csv_report[0]
        host = self.csv_report[2]
        command = self.csv_report[3]

        if not ("read" in self.csv_report[5] and "writ" in self.csv_report[5]):
            self.with_io = False

        print(f"version: {''.join(version)}")
        print(f"Hostname: {host[1]}")
        print(f"Username: {host[-1]}")
        print(f"Commandline: {command[1]}")


    def create_profile(self):
        """
        Create profiling dictionary
        """
        # Get total number of cpus
        self.ncpu = int(self.csv_report[5][-1]) + 1

        # Init dictionaries containing indices
        sys_idx = {}
        io_idx = {}
        net_idx = {}
        cpu_idx = {}

        # Corresponding indices to system columns: "--time"
        sys_idx = {"time": 0}

        # Corresponding indices to memory columns: "--mem --swap"
        mi0 = len(sys_idx)
        mem_labels = ["mem-used", "mem-free", "mem-cach", "mem-avai", "swp-used", "swp-free"]
        mem_idx = {label: mi0 + idx for idx, label in enumerate(mem_labels)}

        # Corresponding indices to io/disk columns "--io --aio --disk --fs"
        ii0 = len(sys_idx) + len(mem_idx)
        io_labels = ["io-async", "fs-file", "fs-inod"]
        if self.with_io:
            io_labels = ["io-read", "io-writ", "io-async", "dsk-read", "dsk-writ", "fs-file", "fs-inod"]
        io_idx = {label: ii0 + idx for idx, label in enumerate(io_labels)}

        # Corresponding indices to network: "--net"
        ni0 = len(sys_idx) + len(mem_idx) + len(io_idx)
        net_labels = ["net-recv", "net-send"]
        net_idx = {label: ni0 + idx for idx, label in enumerate(net_labels)}

        # Corresponding indices to CPU: "--cpu --cpu-use"
        ci0 = len(sys_idx) + len(mem_idx) + len(io_idx) + len(net_idx)
        cpu_labels = ["cpu-usr", "cpu-sys", "cpu-idl", "cpu-wai", "cpu-stl"] + [f"cpu-{i}" for i in range(self.ncpu)]
        cpu_idx = {label: ci0 + idx for idx, label in enumerate(cpu_labels)}

        # Full indices correspondance
        prof_idx = {**sys_idx, **mem_idx, **io_idx, **net_idx, **cpu_idx}
        self.prof_keys = list(prof_idx.keys())

        # Construct full profiling dictionary of list
        for key in self.prof_keys:
            self.prof[key] = []

        # Loop on timestamps (timing starts for index 6)
        for stamp in range(6, len(self.csv_report)):
            # Loop on keys
            for key in self.prof_keys:

                # Skip csv non-informative lines
                if self.csv_report[stamp] == []:
                    continue
                elif self.csv_report[stamp][0] in ("Host:", "Cmdline:", "system", "time"):
                    continue

                # Get value
                val = self.csv_report[stamp][prof_idx[key]]

                # Convert val to float (execpt time)
                if key != "time":
                    val = float(val)

                # Append value to dictonary
                self.prof[key].append(val)

        # Convert list to numpy array
        for key in self.prof_keys:
            self.prof[key] = np.array(self.prof[key])


    def create_plt_params(self):
        """
        Create plot parameters
        """
        nstamps = len(self.prof["time"])
        self._stamps = np.arange(nstamps)
        self._yrange = 10
        xrange = 20
        xticks_val = nstamps / xrange * np.arange(xrange + 1)
        xticks_label = [int(nstamps / xrange * tick) for tick in range(xrange + 1)]
        for i in [0, -1]:
            xticks_label[i] = f"{xticks_label[i]}\n {self.prof['time'][i][:]}"
        self._xticks = (xticks_val, xticks_label)
        self._zeros = np.zeros(nstamps)


    def plot_cpu_average(self):
        """
        Plot average cpu usage
        """
        alpha = .25

        # user-cpu
        plt.fill_between(self._stamps, self._zeros, self.prof["cpu-usr"], color="b", alpha=alpha)
        plt.plot(self._stamps, self.prof["cpu-usr"], "b", label="usr")

        # system-cpu
        plt.fill_between(self._stamps, self._zeros, self.prof["cpu-sys"], color="purple", alpha=alpha)
        plt.plot(self._stamps, self.prof["cpu-sys"], "purple", label="sys")

        # idle-cpu
        plt.fill_between(self._stamps, self._zeros, self.prof["cpu-idl"], color="g", alpha=alpha)
        plt.plot(self._stamps, self.prof["cpu-idl"], "g", label="idle")

        # waiting-cpu
        plt.fill_between(self._stamps, self._zeros, self.prof["cpu-wai"], color="r", alpha=alpha)
        plt.plot(self._stamps, self.prof["cpu-wai"], "r", label="wait")

        # stalled-cpu
        plt.fill_between(self._stamps, self._zeros, self.prof["cpu-stl"], color="orange", alpha=alpha)
        plt.plot(self._stamps, self.prof["cpu-stl"], "orange", label="stalled")

        plt.xticks(self._xticks[0], self._xticks[1])
        plt.yticks(100 / self._yrange * np.arange(self._yrange + 1))
        plt.ylabel("CPU usage (%)")
        plt.xlabel("Timestamp")
        plt.grid()
        plt.legend(loc=1)


    def plot_cpu_per_core(self, with_legend=False, with_color_bar=False, fig=None, nsbp=None, sbp=None):
        """
        Plot cpu per core
        """
        alpha = 0.8
        cm = plt.cm.jet(np.linspace(0, 1, self.ncpu+1))
        cpu_n = self._zeros
        for cpu in range(self.ncpu):
            if cpu > 0:
                cpu_n = cpu_nn
            cpu_nn = self.prof[f"cpu-{cpu}"] / self.ncpu + cpu_n
            plt.fill_between(self._stamps, cpu_n, cpu_nn, color=cm[cpu], alpha=alpha, label=f"cpu-{cpu}")
        plt.xticks(self._xticks[0], self._xticks[1])
        plt.yticks(100 * 1/self._yrange * np.arange(self._yrange + 1))
        plt.ylabel(f" CPU Cores (x{self.ncpu}) (%)")
        plt.grid()
        plt.xlabel("Timestamp")


        if with_legend:
            plt.legend(loc=1, ncol=3)

        if with_color_bar:
            cax = fig.add_axes([0.955, 1 - (sbp-.2)/nsbp, fig.get_figwidth()/1e4, .7/nsbp]) # [left, bottom, width, height]
            plt.colorbar(plt.cm.ScalarMappable(norm=plt.Normalize(vmin=1, vmax=self.ncpu), cmap=plt.cm.jet), \
                         ticks=np.linspace(1, self.ncpu, min(self.ncpu, 5), dtype="i"), cax=cax)


    def plot_memory_usage(self):
        """
        Plot memory usage
        """
        alpha = .3
        mem_unit = 1024 ** 3 # (GB)

        # Total memory
        plt.fill_between(self._stamps, self.prof["mem-used"] / mem_unit, (self.prof["mem-used"] + self.prof["mem-cach"] + self.prof["mem-free"]) / mem_unit, alpha=alpha, label="Total memory", color="b")

        # Used memory
        plt.fill_between(self._stamps, self._zeros, self.prof["mem-used"] / mem_unit, alpha=alpha*3, label="Used memory", color="b")

        # Total swap
        plt.fill_between(self._stamps, self._zeros, (self.prof["swp-used"] + self.prof["swp-free"]) / mem_unit, alpha=alpha, label="Total swap", color="r")

        # Used swap
        plt.fill_between(self._stamps, self._zeros, self.prof["swp-used"] / mem_unit, alpha=alpha*3, label="Used swap", color="r")

        plt.xticks(self._xticks[0], self._xticks[1])
        plt.yticks(np.linspace(0, max((self.prof["mem-used"] + self.prof["mem-cach"] + self.prof["mem-free"]) / mem_unit), 5, dtype="i"))
        plt.ylabel("Memory (GB)")
        plt.legend(loc=1)
        plt.grid()
        plt.xlabel("Timestamp")


    def plot_network(self):
        """
        Plot network activity
        """
        # Yplot: #opened_files - #inodes
        plt.plot(self._stamps, self.prof["fs-file"], "g-", label="# Open file")
        plt.plot(self._stamps, self.prof["fs-inod"], "y-", label="# inodes")
        plt.ylabel("Total number")
        plt.xlabel("Timestamp")
        plt.xticks(self._xticks[0], self._xticks[1])
        plt.legend(loc=2)
        plt.grid()

        # YYplot: download - upload
        alpha = .5
        mem_unit = 1024 ** 2
        pltt = plt.twinx()
        pltt.fill_between(self._stamps, self._zeros, self.prof["net-recv"] / mem_unit, alpha=alpha, color="b", label="Recv")
        pltt.fill_between(self._stamps, self._zeros, self.prof["net-send"] / mem_unit, alpha=alpha, color="r", label="Send")
        pltt.grid()
        pltt.set_ylabel("Network (MB/s)")
        pltt.legend(loc=1)


    def plot_io(self):
        """
        Plot IO stats
        """
        if self.with_io:
            alpha = .5
            mem_unit = 1024 ** 2

            # Yplot: #read-requests; #write-requests; #async-requests
            plt.plot(self._stamps, self.prof["io-read"], "b-", label="#read")
            plt.plot(self._stamps, self.prof["io-writ"], "r-", label="#write")
            plt.plot(self._stamps, self.prof["io-async"], "g-", label="#async")
            plt.ylabel("# Number of requests")
            plt.xticks(self._xticks[0], self._xticks[1])
            plt.legend(loc=2)
            plt.xlabel("Timestamp")
            plt.grid()

            # YYplot: disk-read; disk-write
            pltt = plt.twinx()
            pltt.fill_between(self._stamps, self._zeros, self.prof["dsk-read"] / mem_unit, color="b", alpha=alpha, label="Disk read")
            pltt.fill_between(self._stamps, self._zeros, self.prof["dsk-writ"] / mem_unit, color="r", alpha=alpha, label="Disk write")
            pltt.set_ylabel("IO (MB)")
            pltt.legend(loc=1)
            plt.grid()


def main():
    dvisu = dool_visualizer()

    nsbp = args.cpu + args.cpu_all + args.mem + args.net + args.io
    all_metrics = False
    if nsbp == 0:
        nsbp = 4 + dvisu.with_io
        all_metrics = True

    sp = 1

    figsize = (nsbp*6, nsbp*4)
    fig = plt.figure(figsize=figsize)

    if args.cpu or all_metrics:
        plt.subplot(nsbp, 1, sp); sp += 1
        dvisu.plot_cpu_average()

    if args.cpu_all or all_metrics:
        plt.subplot(nsbp, 1, sp); sp += 1
        dvisu.plot_cpu_per_core(with_color_bar=True, fig=fig, nsbp=nsbp, sbp=2) #with_legend=True)

    if args.mem or all_metrics:
        plt.subplot(nsbp, 1, sp); sp += 1
        dvisu.plot_memory_usage()

    if args.net or all_metrics:
        plt.subplot(nsbp, 1, sp); sp += 1
        dvisu.plot_network()

    if (args.io or all_metrics) and dvisu.with_io:
        plt.subplot(nsbp, 1, sp); sp += 1
        dvisu.plot_io()

    plt.subplots_adjust(hspace=.5)
    plt.tight_layout()

    figname = f"{filename[:-4]}-visu.png"

    dpi = {"low": 200, "medium": 400, "high": 600}
    plt.savefig(figname, dpi=dpi[args.dpi])
    print(f"Figure saved in: {abspath(figname)}")

    plt.show()


if __name__ == "__main__":
    main()
