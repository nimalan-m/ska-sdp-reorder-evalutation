params.out = "$PWD/out"
image_size = 8192
niter = 10000

process dp3 {
    input:
        path datafile
        path dp3_parset
    output:
        path "*.MS"
    script:
    """
    DP3 ${dp3_parset}
    """
}

process dp3_with_reorder {
    input:
        path datafile
        path dp3_parset
    output:
        path '*.MS'
        path '*.tmp'
    script:
    """
    DP3 ${dp3_parset}
    """
}

process ws_clean_imaging {
    publishDir "${params.out}"
    input:
        path datafile
        val resolution
    output:
        path '*.fits'
    script:
    """
    wsclean -no-update-model-required -verbose -reorder  \
        -size ${image_size} ${image_size} -scale ${resolution} -pol QU -mgain 0.85 -niter ${niter}  \
        -auto-threshold 3 -join-polarizations -squared-channel-joining -log-time \
        -no-mf-weighting -name expected_output ${datafile}
    """
}


process ws_clean_save_reorder {
    input:
        path datafile
        val resolution
    output:
        path '*.tmp'
    script:
    """
    echo ${datafile}
    wsclean -no-update-model-required -verbose -save-reorder  \
        -size ${image_size} ${image_size} -scale ${resolution} -pol QU -mgain 0.85 -niter ${niter}  \
        -auto-threshold 3 -join-polarizations -squared-channel-joining  \
        -no-mf-weighting -name expected_output ${datafile}
    """
}

process ws_clean_imaging_with_prereorder {
    publishDir "${params.out}"
    input:
        path datafile
        val resolution
        path reorder_files
    output:
        path '*.fits'
    script:
    """
    wsclean -no-update-model-required -verbose -pre-reorder  \
        -size ${image_size} ${image_size} -scale ${resolution} -pol QU -mgain 0.85 -niter ${niter}  \
        -auto-threshold 3 -join-polarizations -squared-channel-joining -log-time  \
        -no-mf-weighting -name expected_output ${datafile}
    """
}
